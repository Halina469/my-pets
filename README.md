# Exercise 1

## Setup
We are assuming you have already installed Git on your computer.  
If not, go [here](https://git-scm.com/downloads), and download/run the installer.

To do this exercise, you need to copy this code to your computer:
 1. Copy the clone with HTTPS link at the top of the page (blue button)
 2. Open Git Bash terminal
 3. `cd /c/xampp/htdocs` (note the unix-style paths because of the bash shell)
 4. `git clone [clone link]` (this will create a new folder with exercise 1 code)
 5. Open the new exercise folder in VS Code

## Exercise Instructions
See `ex-1.php` for instructions in the comments at the top