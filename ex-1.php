<?php
/**
 * the idea of the exercise is to parse a text file
 * and get the name, type, and age of my pets.
 * 
 * Summarize the data by printing the following:
 * 
 * 1. There are {0} types of pets - 0: quantity
 * 2. For each type of pet print: There are {0} {1} - 0: quantity, 1: type
 * 3. {0} is the oldest pet, age {1} - 0: name, 1: age
 * 4. {0} is the youngest pet, age {1} - 0: name, 1: age
 * 5. The average age of the pets is {0} - 0: average age
 * 
 * some useful functions:
 * - array_push
 * - array_sum
 * - array_unique
 * - array_count_values
 * - usort
 * - printf
 * - number_format
 * - count
 */
// open file
$handle = fopen('pets.txt', 'r');
// an empty array where we will store our pet list
$pets = [];

// loop through each line in the file
while ($line = fgets($handle)) {
    // separate a line into pieces by space character
    $pet = explode(' ', trim($line));
    if (count($pet) === 3) {
        // we have the info we need
        array_push($pets, [
            'name' => $pet[0],
            'type' => $pet[1],
            'age' => $pet[2],
        ]);
    }
}
fclose($handle);

// what should we do with $pets array? 
// remember we need the quantity for each pet type, and average age


// how can we get the quantity of each pet type?


// how can we calculate the average age?


// how can we easily know which pet is the oldest and which is youngest?


exit(0);